####
# This Dockerfile is used in order to build a distroless container that runs the Quarkus application in native (no JVM) mode
#
# Before building the container image run:
# ./mvnw package -Pnative
#
# Then, build the image with:
# docker build -t quarkus/code-with-quarkus .
#
# Then run the container using:
# docker run -i --rm -p 8080:8080 quarkus/code-with-quarkus
#
###
FROM quay.io/quarkus/quarkus-distroless-image:1.0

EXPOSE 8080

ENV quarkus.datasource.db-kind=postgresql \
    quarkus.datasource.username= \
    quarkus.datasource.password= \
    quarkus.datasource.reactive.url= \
    quarkus.oidc.auth-server-url=

COPY target/*-runner /application

USER nonroot

CMD ["./application", "-Dquarkus.http.host=0.0.0.0"]
