# Toranku Project
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=moon-toranku_service&metric=alert_status) ![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=moon-toranku_service&metric=sqale_rating) ![Coverage](https://sonarcloud.io/api/project_badges/measure?project=moon-toranku_service&metric=coverage)](https://sonarcloud.io/summary/new_code?id=moon-toranku_service)

[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/reiizumi/toranku) ![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/reiizumi/toranku) ![Docker Pulls](https://img.shields.io/docker/pulls/reiizumi/toranku)](https://hub.docker.com/r/reiizumi/toranku)
## Introduction
This project allows you to manage categories for different projects and retrieve it by a REST API.

You can find the information on how to use it according to the [OpenAPI](https://gitlab.com/moon-toranku/openapi/-/blob/master/reference/Toranku.yaml).

_URLs indicated in the OpenAPI file are from a private environment and are not accessible_

## Requirements
The data is stored in a Postgres, tables are created automatically if they don't exist.

Access is restricted from OIDC, according to Keycloak and its roles are used. Valid users are according to Client Credentials flow and with the roles indicated in the OpenAPI.

Each system account has its own data.

### Keycloak configuration
The following roles must be created:
- toranku:user 
- toranku:admin

The system accounts that require access to this service must be configured according to:
- **Client authentication**: enabled
- **Authentication flow**: Service account roles
- **Client scopes**: profile, roles
- **Service account roles**: *toranku:user* and/or *toranku:admin*

## Start locally
You can start the service locally to make the necessary modifications. The service is generated from [Quarkus](https://quarkus.io) with Rest Reactive and Database Reactive. 

### Running the application in dev mode
To make code changes, start the code in development mode:
```shell script
./mvnw compile quarkus:dev
```
This mode uses the properties with the `%dev` prefix or those that do not have any prefix in the `resources/application.properties`.

### Creating a native executable
The compilation is done in native using Graal. No need to install Graal in your system, the compilation is done through a container. _This may take several minutes._

```shell script
./mvnw package -Pnative --define quarkus.native.container-build=true
```

You can then execute your native executable with: `./target/toranku-1.0.0-runner`

## Container
The service has been created to be packaged in a container with minimal libraries.

```shell script
docker build -f src/main/docker/dockerfile -t moon-cat/toranku .
```

You can find more container information in [Docker Hub](https://hub.docker.com/r/reiizumi/toranku)

## Helm
Helm chart is available in [Artifact Hub](https://artifacthub.io/packages/helm/toranku/toranku)

## Extra deploy documentation
More documentation (in Spanish) is found in the [Moon.cat](https://www.moon.cat/docs/k8s/services/toranku)

## Quality Check
The code is analyzed according to [SonarCloud](https://sonarcloud.io/project/overview?id=moon-toranku_service)

## Health status and metrics
* `/q/health/ready` indicates whether the service is available for use.
* `/q/metrics` metrics in Prometheus format.
