# toranku-chart



This project allows you to manage categories for different projects and retrieve it by a REST API.

![Version: 1.1.2](https://img.shields.io/badge/Version-1.1.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.1.0](https://img.shields.io/badge/AppVersion-1.1.0-informational?style=flat-square) 

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm upgrade --install my-release toranku-chart -f toranku-chart-values.yaml -n toranku
```

This `toranku-chart-values.yaml` has the most typical configurations:
```yaml
ingress:
  enabled: true
  url: toranku.domain.cat
  tls: true
autoscaling:
  enabled: true

database:
  url: postgresql://toranku-pg-cluster-rw.toranku:5432/toranku
  auth:
    secretName: toranku-pg-cluster-app
    usernameKey: user
    passwordKey: password

oidc:
  url: https://auth.domain.cat/realms/Intranet
```

## Uninstalling the Chart
To uninstall/delete the my-release deployment:

```console
$ helm delete my-release -n toranku
```

This will delete all components.



## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| image.repository | string | `"reiizumi/toranku"` | Name of the image |
| image.pullPolicy | string | `"IfNotPresent"` | Download the image only if not exist (IfNotPresent) or always (Always) |
| image.tag | string | `""` | Overrides the image tag defined in the Chart |
| nameOverride | string | `""` |  |
| fullnameOverride | string | `""` |  |
| imagePullSecrets | list | `[]` |  |
| service.type | string | `"ClusterIP"` |  |
| service.port | int | `8080` |  |
| ingress.enabled | bool | `false` | Enable Ingress |
| ingress.url | string | `"toranku.domain.cat"` | URL to publish the service |
| ingress.className | string | `""` | Ingress class name. Empty to use the default one |
| ingress.annotations | object | `{"cert-manager.io/cluster-issuer":"letsencrypt-http"}` | Annotations to configure the Ingress |
| ingress.tls | bool | `false` | Enable the TLS configuration |
| ingress.path | string | `"/"` | Limit access to the endpoints according to a base path |
| ingress.pathType | string | `"ImplementationSpecific"` | Type of path route |
| podAnnotations | object | `{}` |  |
| podLabels | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| securityContext | object | `{}` |  |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.automount | bool | `true` | Automatically mount a ServiceAccount's API credentials? |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| nodeSelector | object | `{}` |  |
| tolerations | list | `[]` |  |
| affinity | object | `{}` |  |
| replicaCount | int | `1` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.maxReplicas | int | `5` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` |  |
| resources | object | `{"limits":{"cpu":0.2,"memory":"128Mi"},"requests":{"cpu":0.1,"memory":"64Mi"}}` | CPU and memory required and limits for the service |
| livenessProbe | object | `{"httpGet":{"path":"/q/health/live","port":"http"}}` | Checks if the service is alive and running  |
| readinessProbe | object | `{"httpGet":{"path":"/q/health/ready","port":"http"}}` | Checks if the service is ready to accept traffic  |
| volumes | list | `[]` | Additional volumes on the output Deployment definition. |
| volumeMounts | list | `[]` | Additional volumeMounts on the output Deployment definition. |
| database.url | string | `"postgresql://toranku-pg-cluster-rw.toranku:5432/toranku"` | PostgreSQL JDBC URL (according to Reactive JDBC) |
| database.auth.secretName | string | `"toranku-pg-cluster-app"` | Secret name where the username and password are stored |
| database.auth.usernameKey | string | `"user"` | Key to get the username from the secret |
| database.auth.passwordKey | string | `"password"` | Key to get the password from the secret |
| oidc.url | string | `"https://auth.domain.cat/realms/Intranet"` | Identity Provider URL used to authenticate the services |
| log.level | string | `"INFO"` | Log level |
| log.sql | bool | `false` | Enable SQL logging |
| extraEnvs | object | `{}` | Add extra environment variables |
