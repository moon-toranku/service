{{ template "chart.header" . }}
{{ template "chart.deprecationWarning" . }}

{{ template "chart.description" . }}

{{ template "chart.badgesSection" . }}

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm upgrade --install my-release {{ template "chart.name" . }} -f {{ template "chart.name" . }}-values.yaml -n toranku
```

This `{{ template "chart.name" . }}-values.yaml` has the most typical configurations:
```yaml
ingress:
  enabled: true
  url: toranku.domain.cat
  tls: true
autoscaling:
  enabled: true

database:
  url: postgresql://toranku-pg-cluster-rw.toranku:5432/toranku
  auth:
    secretName: toranku-pg-cluster-app
    usernameKey: user
    passwordKey: password

oidc:
  url: https://auth.domain.cat/realms/Intranet
```

## Uninstalling the Chart
To uninstall/delete the my-release deployment:

```console
$ helm delete my-release -n toranku
```

This will delete all components.

{{ template "chart.requirementsSection" . }}

{{ template "chart.valuesSection" . }}
