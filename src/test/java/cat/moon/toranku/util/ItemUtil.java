package cat.moon.toranku.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Pattern;

import javax.enterprise.context.ApplicationScoped;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import cat.moon.toranku.entity.Category;
import cat.moon.toranku.entity.Project;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@ApplicationScoped
public class ItemUtil {
    public final String fakeUser = "fakeUser";
    public final String token = AccessTokenUtil.get(fakeUser, new HashSet<>(Arrays.asList("toranku:user")));
    public final String adminToken = AccessTokenUtil.get("fakeadmin", new HashSet<>(Arrays.asList("toranku:admin")));

    public final String urlProject = "/api/v1/projects";

    public final String urlCategory = "/api/v1/categories";
    public final String urlCategoryParam = "id";
    public final String urlCategoryById = urlCategory+"/{"+urlCategoryParam+"}";
    
    public final String urlTag = "/api/v1/tags";
    public final String urlTagParam = "name";
    public final String urlTagByName = urlTag+"/{"+urlTagParam+"}";

    public final String urlAdminProject = "/api/v1/admin/projects";
    public final String urlUserIdParam = "user-id";
    public final String urlAdminProjectByUserId = urlAdminProject+"/{"+urlUserIdParam+"}";
    public final String urlAdminCategoryByUserId = "/api/v1/admin/categories/{"+urlUserIdParam+"}";
    public final String urlAdminCategoryByUserIdAndId = urlAdminCategoryByUserId+"/{"+urlCategoryParam+"}";

    public final String idMatcher = "[\\d]+$";
    public final String domain = "http[s]?:\\/\\/[a-zA-Z\\d:.@]+";
    public final Matcher<String> urlProjectMatcher = Matchers.matchesPattern(domain+"\\/api\\/v1\\/projects");
    public final Matcher<String> urlCategoryMatcher = Matchers.matchesPattern(domain+"\\/api\\/v1\\/categories\\/"+idMatcher);
    public final Matcher<String> urlAdminProjectMatcher = Matchers.matchesPattern(domain+"\\/api\\/v1\\/admin\\/projects");
    public final Matcher<String> urlAdminCategoryMatcher = Matchers.matchesPattern(domain+"\\/api\\/v1\\/admin\\/categories\\/[\\w\\d_-]+\\/"+idMatcher);
    
    public Project createProject() {
        Project item = new Project();
        item.name = "Fake project";
        item.description = "Fake description";
        return item;
    }

    public Category createCategory() {
        Category item = new Category();
        item.name = "House";
        item.description = "Household expenses";
        item.icon = "/home.ico";
        item.sort = 3;
        item.tags = Arrays.asList("home", "house");
        return item;
    }

    public Long retrieveId(String location) {
        Pattern p = Pattern.compile(idMatcher);
        java.util.regex.Matcher m = p.matcher(location);
        assertTrue(m.find());
        Long id = Long.parseLong(m.group());
        assertNotNull(id);
        return id;
    }

    public Long createCategory(Category item, String token) {
        Response response = RestAssured.given()
            .auth().oauth2(token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(urlCategory)
            .then().extract().response();
        String location = response.header("Location");
        assertThat(response.statusCode(), is(201));
        assertThat(location, urlCategoryMatcher);
        return retrieveId(location);
    }

    public void compare(Category c, Category item) {
        assertThat(c.id, is(item.id));
        assertThat(c.name, is(item.name));
        assertThat(c.description, is(item.description));
        assertThat(c.icon, is(item.icon));
        assertThat(c.sort, is(item.sort));
        assertThat(c.tags, everyItem(is(in(item.tags))));
    }
}
