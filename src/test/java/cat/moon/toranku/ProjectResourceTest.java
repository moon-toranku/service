package cat.moon.toranku;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.toranku.entity.Project;
import cat.moon.toranku.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty, 400 and 404
- Order 2: Create project
- Order 3: Get, create conflict
- Order 4: Put
- Order 5: Delete
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class ProjectResourceTest {

    @Inject
    ItemUtil util;

    @Test
    @Order(3)
    void getV1Project200() {
        Log.info("Test - Get Project");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("name"), is("Fake project"));
        assertThat(response.jsonPath().getString("description"), is("Fake description"));
    }

    @Test
    @Order(1)
    void getV1Project404() {
        Log.info("Test - Get Project - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlProject)
            .then().extract().response();        
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void postV1Project400() {
        Log.info("Test - Create Project - Bad request");
        Project item  = util.createProject();
        item.name = null;
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("violations").isEmpty());
        assertThat(response.jsonPath().getString("violations[0]"), is("[field:create.project.name, message:Name may not be blank]"));
    }

    @Test
    @Order(2)
    void postV1Project201() {
        Log.info("Test - Create Project");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.createProject())
            .when().post(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), util.urlProjectMatcher);
    }

    @Test
    @Order(3)
    void postV1Project409() {
        Log.info("Test - Create Project - Exist - Conflict");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.createProject())
            .when().post(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(409));
        assertThat(response.header("Location"), util.urlProjectMatcher);
    }

    @Test
    @Order(4)
    void putV1Project204() {
        Log.info("Test - Update Project");
        Project item  = util.createProject();
        item.name += " updated";
        item.description += " updated";
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().put(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
        assertThat(response.header("Location"), util.urlProjectMatcher);
        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("name"), is(item.name));
        assertThat(response.jsonPath().getString("description"), is(item.description));
    }

    @Test
    @Order(1)
    void putV1Project404() {
        Log.info("Test - Update Project - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.createProject())
            .when().put(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void putV1Project400() {
        Log.info("Test - Update Project - Bad request");
        Project item  = new Project();
        item.name = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        item.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lectus dolor, pharetra sed nisi nec, finibus sodales velit. Aenean fringilla egestas massa quis tincidunt. Phasellus quis leo diam. Aenean commodo arcu mi, a laoreet enim egestas et. Maecenas et diam iaculis, facilisis turpis sed, dignissim tortor. Morbi elementum condimentum nulla, et faucibus massa volutpat sit amet. Pellentesque feugiat nunc urna, id placerat nulla iaculis eu. Duis fermentum pellentesque est, quis sodales justo hendrerit sagittis. Mauris tempor sollicitudin iaculis. Praesent at dui at libero sagittis condimentum. Morbi pellentesque volutpat dolor vitae iaculis. Vivamus quis dui odio. Duis auctor massa augue, quis lacinia ipsum efficitur eget. Suspendisse vel ligula facilisis, ullamcorper odio nec, tincidunt nunc. Donec erat ante, consequat in nulla in, imperdiet volutpat nunc. Cras sit amet massa maximus augue porta commodo. Ut nec nibh a eros tincidunt congue id vel felis. Mauris id vehicula nisl. In metus libero, volutpat sit amet sagittis in, ullamcorper sit amet justo.";
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().put(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("violations").isEmpty());
        assertThat(response.jsonPath().getList("violations").size(), is(2));

        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("violations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                is("{field=update.project.description, message=Description cannot be greater than 1000}"),
                is("{field=update.project.name, message=Name cannot be greater than 50}")
            ));
        }
    }

    @Test
    @Order(5)
    void deleteV1Project200() {
        Log.info("Test - Delete Project");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().delete(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlProject)
            .then().extract().response();        
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void deleteV1Project404() {
        Log.info("Test - Delete Project - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().delete(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }
}
