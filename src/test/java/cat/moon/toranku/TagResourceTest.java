package cat.moon.toranku;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.toranku.entity.Category;
import cat.moon.toranku.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty and 404
- Order 2: Create project and category
- Order 3: Get
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class TagResourceTest {
    @Inject
    ItemUtil util;

    @Test
    @Order(99)
    void clean() {
        Log.info("Test - Clean Tags");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().delete(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
    }

    @Test
    @Order(2)
    void init() {
        Log.info("Test - Tags - Init");
        //Create project
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.createProject())
            .when().post(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        
        //Create category
        response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.createCategory())
            .when().post(util.urlCategory)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
    }

    @Test
    @Order(1)
    void getV1AllTags200Empty() {
        Log.info("Test - Get all tags - Empty");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlTag)
            .then().extract().response();        
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("."), is("[]"));
    }

    @Test
    @Order(3)
    void getV1AllTags200() {
        Log.info("Test - Get all tags");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlTag)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        List<String> tags = response.body().jsonPath().getList(".", String.class);
        assertThat(tags, is(not(empty())));
        Category item = util.createCategory();
        assertThat(tags, containsInAnyOrder(item.tags.toArray()));
    }

    @Test
    @Order(3)
    void getV1Tag200() {
        Log.info("Test - Get Tags");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlTagParam, "home").get(util.urlTagByName)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        List<Category> categories = response.body().jsonPath().getList(".", Category.class);
        assertThat(categories, is(not(empty())));
        Category c = categories.get(0);
        Category item = util.createCategory();
        item.id = c.id;
        util.compare(c, item);
    }
}
