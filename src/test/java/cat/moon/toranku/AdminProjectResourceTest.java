package cat.moon.toranku;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.toranku.entity.Project;
import cat.moon.toranku.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty, 400 and 404
- Order 2: Create project and get
- Order 3: Get
- Order 4: Update
- Order 5: Delete
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class AdminProjectResourceTest {
    private final String notExistingUser = "notExistingUser";

    @Inject
    ItemUtil util;

    @Test
    @Order(2)
    void getV1AdminAllProjects200() {
        Log.info("Test - Admin - Get all projects");
        //Create project
        Project item = util.createProject();
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), util.urlProjectMatcher);
        
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().get(util.urlAdminProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Log.info(response.body().jsonPath());
        List<Project> project = response.body().jsonPath().getList(".", Project.class);
        assertThat(project, is(not(empty())));

        Optional<Project> responseCategory = project.stream().filter(p -> p.userId.equals(util.fakeUser)).findFirst();
        assertThat(responseCategory, isPresent());

        assertThat(responseCategory.get().name, is(item.name));
        assertThat(responseCategory.get().description, is(item.description));
    }

    @Test
    @Order(3)
    void getV1AdminProject200() {
        Log.info("Test - Admin - Get Project");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).get(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("name"), is("Fake project"));
        assertThat(response.jsonPath().getString("description"), is("Fake description"));
    }

    @Test
    @Order(1)
    void getV1AdminProject404() {
        Log.info("Test - Admin - Get Project - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, notExistingUser).get(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(4)
    void putV1AdminProject204() {
        Log.info("Test - Admin - Update Project");
        Project item  = util.createProject();
        item.name += " updated";
        item.description += " updated";
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).put(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
        assertThat(response.header("Location"), util.urlAdminProjectMatcher);
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).get(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("name"), is(item.name));
        assertThat(response.jsonPath().getString("description"), is(item.description));
    }

    @Test
    @Order(1)
    void putV1AdminProject404() {
        Log.info("Test - Admin - Update Project - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(util.createProject())
            .when().pathParam(util.urlUserIdParam, notExistingUser).put(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void putV1AdminProject400() {
        Log.info("Test - Admin - Update Project - Bad request");
        Project item  = new Project();
        item.name = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        item.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lectus dolor, pharetra sed nisi nec, finibus sodales velit. Aenean fringilla egestas massa quis tincidunt. Phasellus quis leo diam. Aenean commodo arcu mi, a laoreet enim egestas et. Maecenas et diam iaculis, facilisis turpis sed, dignissim tortor. Morbi elementum condimentum nulla, et faucibus massa volutpat sit amet. Pellentesque feugiat nunc urna, id placerat nulla iaculis eu. Duis fermentum pellentesque est, quis sodales justo hendrerit sagittis. Mauris tempor sollicitudin iaculis. Praesent at dui at libero sagittis condimentum. Morbi pellentesque volutpat dolor vitae iaculis. Vivamus quis dui odio. Duis auctor massa augue, quis lacinia ipsum efficitur eget. Suspendisse vel ligula facilisis, ullamcorper odio nec, tincidunt nunc. Donec erat ante, consequat in nulla in, imperdiet volutpat nunc. Cras sit amet massa maximus augue porta commodo. Ut nec nibh a eros tincidunt congue id vel felis. Mauris id vehicula nisl. In metus libero, volutpat sit amet sagittis in, ullamcorper sit amet justo.";
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).put(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("violations").isEmpty());
        assertThat(response.jsonPath().getList("violations").size(), is(2));

        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("violations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                is("{field=update.project.description, message=Description cannot be greater than 1000}"),
                is("{field=update.project.name, message=Name cannot be greater than 50}")
            ));
        }
    }

    @Test
    @Order(5)
    void deleteV1Project200() {
        Log.info("Test - Delete Project");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).delete(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).get(util.urlAdminProjectByUserId)
            .then().extract().response();        
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void deleteV1Project404() {
        Log.info("Test - Delete Project - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, notExistingUser).delete(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }
}
