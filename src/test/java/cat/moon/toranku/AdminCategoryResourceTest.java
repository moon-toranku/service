package cat.moon.toranku;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.toranku.entity.Category;
import cat.moon.toranku.entity.Project;
import cat.moon.toranku.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty, 400 and 404
- Order 2: Create project and category
- Order 3: Others
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class AdminCategoryResourceTest {
    private final String notExistingUser = "notExistingUser";

    @Inject
    ItemUtil util;

    @Test
    @Order(99)
    void clean() {
        Log.info("Test - Admin Clean Category");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).delete(util.urlAdminProjectByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
    }

    @Test
    @Order(1)
    void getV1AdminAllCategories200Empty() {
        Log.info("Test - Admin - Get all categories - Empty");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, notExistingUser).get(util.urlAdminCategoryByUserId)
            .then().extract().response();        
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("."), is("[]"));
    }

    @Test
    @Order(3)
    void getV1AdminAllCategories200() {
        Log.info("Test - Admin - Get all categories");
        Category item = util.createCategory();
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).post(util.urlAdminCategoryByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), util.urlAdminCategoryMatcher);
        item.id = util.retrieveId(response.header("Location"));

        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).get(util.urlAdminCategoryByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Log.info(response.body().jsonPath());
        List<Category> categories = response.body().jsonPath().getList(".", Category.class);
        assertThat(categories, is(not(empty())));

        Optional<Category> responseCategory = categories.stream().filter(c -> c.id.equals(item.id)).findFirst();
        assertThat(responseCategory, isPresent());
        assertThat(responseCategory.get().name, is(item.name));
        assertThat(responseCategory.get().description, is(item.description));
    }

    @Test
    @Order(2)
    void postV1AdminCategory201_getV1AdminCategory200() {
        Log.info("Test - Admin - Create and Get Category");
        //Create project
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.createProject())
            .when().post(util.urlProject)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        
        //Create category
        Category item = util.createCategory();
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).post(util.urlAdminCategoryByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), util.urlAdminCategoryMatcher);

        //Check category values
        item.id = util.retrieveId(response.header("Location"));
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser)
                   .pathParam(util.urlCategoryParam, item.id).get(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Category c = response.as(Category.class);
        util.compare(c, item);
    }

    @Test
    @Order(1)
    void postV1AdminCategory400() {
        Log.info("Test - Admin - Create Category - Bad request");
        Category item  = util.createCategory();
        item.name = null;
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, notExistingUser).post(util.urlAdminCategoryByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("violations").isEmpty());
        assertThat(response.jsonPath().getString("violations[0]"), is("[field:create.category.name, message:Name may not be blank]"));
    }

    @Test
    @Order(1)
    void postV1AdminCategory409() {
        Log.info("Test - Admin - Create Category - Project not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(util.createCategory())
            .when().pathParam(util.urlUserIdParam, util.fakeUser).post(util.urlAdminCategoryByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(409));
    }
    
    @Test
    @Order(1)
    void getV1AdminCategory404() {
        Log.info("Test - Admin - Get Category - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, notExistingUser)
                   .pathParam(util.urlCategoryParam, 99999).get(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(3)
    void putV1AdminCategory204() {
        Log.info("Test - Admin - Update Project");
        //Create category
        Category item = util.createCategory();
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).post(util.urlAdminCategoryByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), util.urlAdminCategoryMatcher);
        item.id = util.retrieveId(response.header("Location"));

        item.name += " updated";
        item.description += " updated";
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser)
                   .pathParam(util.urlCategoryParam, item.id).put(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
        assertThat(response.header("Location"), is(util.urlAdminCategoryMatcher));
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser)
                   .pathParam(util.urlCategoryParam, item.id).get(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("name"), is(item.name));
        assertThat(response.jsonPath().getString("description"), is(item.description));
    }

    @Test
    @Order(1)
    void putV1AdminCategory404() {
        Log.info("Test - Admin - Update Category - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(util.createCategory())
            .when().pathParam(util.urlUserIdParam, notExistingUser)
                   .pathParam(util.urlCategoryParam, 99999).put(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void putV1AdminCategory400() {
        Log.info("Test - Admin - Update Category - Bad request");
        Project item  = new Project();
        item.name = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lectus dolor, pharetra sed nisi nec, finibus sodales velit. Aenean fringilla egestas massa quis tincidunt. Phasellus quis leo diam. Aenean commodo arcu mi, a laoreet enim egestas et. Maecenas et diam iaculis, facilisis turpis sed, dignissim tortor. Morbi elementum condimentum nulla, et faucibus massa volutpat sit amet. Pellentesque feugiat nunc urna, id placerat nulla iaculis eu. Duis fermentum pellentesque est, quis sodales justo hendrerit sagittis. Mauris tempor sollicitudin iaculis. Praesent at dui at libero sagittis condimentum. Morbi pellentesque volutpat dolor vitae iaculis. Vivamus quis dui odio. Duis auctor massa augue, quis lacinia ipsum efficitur eget. Suspendisse vel ligula facilisis, ullamcorper odio nec, tincidunt nunc. Donec erat ante, consequat in nulla in, imperdiet volutpat nunc. Cras sit amet massa maximus augue porta commodo. Ut nec nibh a eros tincidunt congue id vel felis. Mauris id vehicula nisl. In metus libero, volutpat sit amet sagittis in, ullamcorper sit amet justo.";
        item.description = item.name;
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser)
                   .pathParam(util.urlCategoryParam, 99999).put(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("violations").isEmpty());
        assertThat(response.jsonPath().getList("violations").size(), is(2));

        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("violations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                is("{field=update.category.description, message=Description cannot be greater than 1000}"),
                is("{field=update.category.name, message=Name cannot be greater than 100}")
            ));
        }
    }

    @Test
    @Order(3)
    void deleteV1AdminCategory204() {
        Log.info("Test - Admin - Delete Category");
        //Create category
        Category item = util.createCategory();
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlUserIdParam, util.fakeUser).post(util.urlAdminCategoryByUserId)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), util.urlAdminCategoryMatcher);
        item.id = util.retrieveId(response.header("Location"));

        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser)
                   .pathParam(util.urlCategoryParam, item.id).delete(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
        response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, util.fakeUser)
                   .pathParam(util.urlCategoryParam, item.id).get(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();        
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void deleteV1AdminCategory404() {
        Log.info("Test - Admin - Delete Category - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.adminToken)
            .when().pathParam(util.urlUserIdParam, notExistingUser)
                   .pathParam(util.urlCategoryParam, 99999).delete(util.urlAdminCategoryByUserIdAndId)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }
}
