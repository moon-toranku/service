package cat.moon.toranku;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.reactive.RestPath;

import cat.moon.toranku.entity.Project;
import cat.moon.toranku.service.ProjectService;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;

@Path("/api/v1/admin/projects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed("toranku:admin")
@ApplicationScoped
public class AdminProjectResource {
    @SuppressWarnings("squid:S1075")
    private static final String BASEPATH = "/api/v1/admin/projects";

    @Inject
    SecurityIdentity identity;
    
    @Inject
    ProjectService service;

    @GET
    public Uni<List<Project>> getAll() {
        return service.getAll();
    }

    @GET
    @Path("/{user-id}")
    public Uni<Response> get(@RestPath("user-id") String userId) {
        return service.get(userId);
    }

    @PUT
    @Path("/{user-id}")
    public Uni<Response> update(@RestPath("user-id") String userId, @Valid @NotNull(message = "Request must be a Project object") Project project) {
        return service.update(userId, project, BASEPATH);
    }

    @DELETE
    @Path("/{user-id}")
    public Uni<Object> delete(@RestPath("user-id") String userId) {
        return service.delete(userId);
    }
}
