package cat.moon.toranku;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.reactive.RestPath;

import cat.moon.toranku.entity.Category;
import cat.moon.toranku.service.CategoryService;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;

@Path("/api/v1/tags")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed("toranku:user")
@ApplicationScoped
public class TagResource {

    @Inject
    SecurityIdentity identity;

    @Inject
    CategoryService service;
    
    @GET
    public Uni<List<String>> getAll() {
        //Multi<String> does not correct a valid JSON, https://github.com/quarkusio/quarkus/issues/18043.
        //Uni<List<String>> works correctly.
        return service.getAllTags(identity.getPrincipal().getName()).collect().asList();
    }

    @GET
    @Path("/{name}")
    public Uni<List<Category>> get(@RestPath String name) {
        return service.getByTag(identity.getPrincipal().getName(), name);
    }
}
