package cat.moon.toranku;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import cat.moon.toranku.entity.Project;
import cat.moon.toranku.service.ProjectService;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;

@Path("/api/v1/projects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed("toranku:user")
@ApplicationScoped
public class ProjectResource {
    @SuppressWarnings("squid:S1075")
    private static final String BASEPATH = "/api/v1/projects";

    @Inject
    SecurityIdentity identity;

    @Inject
    ProjectService service;
    
    @GET
    public Uni<Response> get() {
        return service.get(identity.getPrincipal().getName());
    }

    @POST
    public Uni<Response> create(@Valid Project project) {
        project.userId = identity.getPrincipal().getName();
        return service.create(project, BASEPATH);
    }

    @PUT
    public Uni<Response> update(@Valid @NotNull(message = "Request must be a Project object") Project project) {
        return service.update(identity.getPrincipal().getName(), project, BASEPATH);
    }

    @DELETE
    public Uni<Object> delete() {
        return service.delete(identity.getPrincipal().getName());
    }
}
