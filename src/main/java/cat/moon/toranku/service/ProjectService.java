package cat.moon.toranku.service;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cat.moon.toranku.entity.Project;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.logging.Log;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;

@ApplicationScoped
@SuppressWarnings("squid:S3252")
public class ProjectService {
    public Uni<List<Project>> getAll() {
        return Project.listAll(Sort.by("name"));
    }

    public Uni<Response> get(String userId) {
        return Project.findById(userId)
            .onItem().ifNotNull().transform(project -> Response.ok(project).build())
            .onItem().ifNull().continueWith(Response.status(Status.NOT_FOUND).build());
    }

    public Uni<Response> create(Project project, String location) {
        return Project.findById(project.userId)
            .onItem().ifNotNull().transform(item -> {
                if(Log.isDebugEnabled()) Log.debug("Project exist, discard creation process");
                return Response.status(Status.CONFLICT).location(URI.create(location)).build();
            })
            .onItem().ifNull().switchTo(() -> {
                if(Log.isDebugEnabled()) Log.debug("Project doesn't exist. Creating project");
                return Panache.<Project>withTransaction(project::persist)
                    .onItem().transform(inserted -> Response.created(URI.create(location)).build());
            });
    }

    public Uni<Response> update(String userId, Project project, String location) {
        if(project == null) return Uni.createFrom().item(Response.status(Status.BAD_REQUEST).build());

        return Panache.withTransaction(() -> Project.<Project> findById(userId)
                .onItem().ifNotNull().invoke(entity -> {
                    entity.name = project.name;
                    entity.description = project.description;
                })
            )
            .onItem().ifNotNull().transform(entity -> Response.noContent().location(URI.create(location)).build())
            .onItem().ifNull().continueWith(Response.status(Status.NOT_FOUND).build());
    }

    public Uni<Object> delete(String userId) {
        return Panache.withTransaction(() -> Project.deleteById(userId))
                .map(deleted -> deleted.booleanValue()
                        ? Response.noContent().build()
                        : Response.status(Status.NOT_FOUND).build());
    }
}
