package cat.moon.toranku.service;

import java.net.URI;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cat.moon.toranku.entity.Category;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.sqlclient.Tuple;

@ApplicationScoped
@SuppressWarnings("squid:S3252")
public class CategoryService {
    @Inject
    io.vertx.mutiny.pgclient.PgPool client;

    public Uni<List<Category>> getAll(String userId) {
        return Category.find("project_userid", Sort.by("sort"), userId).list();
    }

    public Uni<Response> get(String userId, long id) {
        return Category.findByUserIdAndId(userId, id)
            .onItem().ifNotNull().transform(category -> Response.ok(category).build())
            .onItem().ifNull().continueWith(Response.status(Status.NOT_FOUND).build());
    }

    public Uni<Response> create(Category category, String location) {
        return Panache.<Category>withTransaction(category::persist)
        .onItem().transform(inserted -> Response.created(URI.create(location+category.id)).build())
        .onFailure(IllegalStateException.class).recoverWithUni(Uni.createFrom().item(Response.status(Status.CONFLICT).build()));
}

    public Uni<Response> update(String userId, long id, Category category, String location) {
        if(category == null) return Uni.createFrom().item(Response.status(Status.BAD_REQUEST).build());
        return Panache.withTransaction(() -> Category.findByUserIdAndId(userId, id)
                .onItem().ifNotNull().invoke(entity -> {
                    entity.name = category.name;
                    entity.description = category.description;
                    entity.icon = category.icon;
                    entity.sort = category.sort;
                    entity.tags = category.tags;
                    entity.project.userId = userId;
                })
            )
            .onItem().ifNotNull().transform(entity -> Response.noContent().location(URI.create(location+category.id)).build())
            .onItem().ifNull().continueWith(Response.status(Status.NOT_FOUND).build());
    }

    public Uni<Object> delete(String userId, long id) {
        return Category.deleteByUserIdAndId(userId, id)
            .map(deleted -> 
                deleted != 0
                    ? Response.noContent().build()
                    : Response.status(Status.NOT_FOUND).build());
    }

    public Multi<String> getAllTags(String userId) {
        return client.preparedQuery("SELECT distinct(t.tags) FROM category c JOIN category_tags t ON c.id = t.category_id WHERE c.project_userid = $1").execute(Tuple.of(userId)) 
            .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
            .onItem().transform(t -> t.getString("tags"));
            
    }

    public Uni<List<Category>> getByTag(String userId, String name) {
        return Category.findByUserIdAndTags(userId, name);
    }
}
