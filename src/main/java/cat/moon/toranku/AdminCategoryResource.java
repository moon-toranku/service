package cat.moon.toranku;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.reactive.RestPath;

import cat.moon.toranku.entity.Category;
import cat.moon.toranku.entity.Project;
import cat.moon.toranku.service.CategoryService;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;

@Path("/api/v1/admin/categories/{user-id}")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed("toranku:admin")
@ApplicationScoped
public class AdminCategoryResource {
    @SuppressWarnings("squid:S1075")
    private static final String GET_BY_ID = "/api/v1/admin/categories/";

    @Inject
    SecurityIdentity identity;
    
    @Inject
    CategoryService service;

    @GET
    public Uni<List<Category>> getAll(@RestPath("user-id") String userId) {
        return service.getAll(userId);
    }

    @POST
    public Uni<Response> create(@RestPath("user-id") String userId, @Valid Category category) {
        if(category.project == null) category.project = new Project();
        category.project.userId = userId;
        return service.create(category, GET_BY_ID+userId+"/");
    }
    
    @GET
    @Path("/{id}")
    public Uni<Response> get(@RestPath("user-id") String userId, @RestPath long id) {
        return service.get(userId, id);
    }

    @PUT
    @Path("/{id}")
    public Uni<Response> update(@RestPath("user-id") String userId, @RestPath long id, @Valid @NotNull(message = "Request must be a Category object") Category category) {
        return service.update(userId, id, category, GET_BY_ID+userId+"/");
    }

    @DELETE
    @Path("/{id}")
    public Uni<Object> delete(@RestPath("user-id") String userId, @RestPath long id) {
        return service.delete(userId, id);
    }
}
