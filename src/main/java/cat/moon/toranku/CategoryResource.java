package cat.moon.toranku;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.reactive.RestPath;

import cat.moon.toranku.entity.Category;
import cat.moon.toranku.entity.Project;
import cat.moon.toranku.service.CategoryService;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;

@Path("/api/v1/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed("toranku:user")
@ApplicationScoped
public class CategoryResource {
    @SuppressWarnings("squid:S1075")
    private static final String GET_BY_ID = "/api/v1/categories/";

    @Inject
    SecurityIdentity identity;

    @Inject
    CategoryService service;
    
    @GET
    public Uni<List<Category>> getAll() {
        return service.getAll(identity.getPrincipal().getName());
    }

    @GET
    @Path("/{id}")
    public Uni<Response> get(@RestPath long id) {
        return service.get(identity.getPrincipal().getName(), id);
    }

    @POST
    public Uni<Response> create(@Valid Category category) {
        if(category.project == null) category.project = new Project();
        category.project.userId = identity.getPrincipal().getName();
        return service.create(category, GET_BY_ID);
    }

    @PUT
    @Path("/{id}")
    public Uni<Response> update(@RestPath long id, @Valid @NotNull(message = "Request must be a Category object") Category category) {
        return service.update(identity.getPrincipal().getName(), id, category, GET_BY_ID);
    }

    @DELETE
    @Path("/{id}")
    public Uni<Object> delete(@RestPath long id) {
        return service.delete(identity.getPrincipal().getName(), id);
    }
}
