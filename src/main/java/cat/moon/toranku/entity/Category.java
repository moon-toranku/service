package cat.moon.toranku.entity;

import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;
import io.quarkus.panache.common.Parameters;
import io.smallrye.mutiny.Uni;

@Entity
@NamedQuery(name = "Category.getByUserIdAndId", query = "FROM Category WHERE project_userId = :userid AND id = :id")
@NamedQuery(name = "Category.deleteByUserIdAndId", query = "DELETE FROM Category WHERE project_userId = :userid AND id = :id")
@NamedQuery(name = "Category.getByUserIdAndTag", query = "FROM Category c JOIN c.tags t WHERE c.project.userId = :userid AND t = :tags")
@NamedQuery(name = "Tags.getByUserId", query = "Select distinct(t) FROM Category c JOIN c.tags t WHERE c.project.userId = :userid")
@Cacheable
@SuppressWarnings({"squid:S1104", "squid:S3252"})
public class Category extends PanacheEntity {
    private static final String PARAM_USERID= "userid";
    private static final String PARAM_ID    = "id";
    private static final String PARAM_TAGS  = "tags";

    @Column(length = 100, nullable = false)
    @NotBlank(message = "Name may not be blank")
    @Size(message="Name cannot be greater than 100", max = 100)
    public String name;

    @Column(length = 1000)
    @Size(message = "Description cannot be greater than 1000", max = 1000)
    public String description;

    @Lob
    public String icon;

    public int sort;

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinColumn(foreignKey = @ForeignKey(foreignKeyDefinition = "FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE CASCADE"))
    public List<String> tags;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    public Project project;

    public static Uni<Category> findByUserIdAndId(String userId, long id) {
        Map<String, Object> params = Parameters
            .with(PARAM_USERID, userId)
            .and(PARAM_ID, id)
            .map();
        return Category.find("#Category.getByUserIdAndId", params).firstResult();
    }

    public static Uni<List<Category>> findByUserIdAndTags(String userId, String tag) {
        Map<String, Object> params = Parameters
            .with(PARAM_TAGS, tag)
            .and(PARAM_USERID, userId)
            .map();
        return Category.find("#Category.getByUserIdAndTag", params).list();
    }

    public static Uni<Long> deleteByUserIdAndId(String userId, long id) {
        Map<String, Object> params = Parameters
            .with(PARAM_USERID, userId)
            .and(PARAM_ID, id)
            .map();
        return Category.delete("#Category.deleteByUserIdAndId", params);
    }
}
