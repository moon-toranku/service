package cat.moon.toranku.entity;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;

@Entity
@Cacheable
@SuppressWarnings("squid:S1104")
public class Project extends PanacheEntityBase {
    @Id
    public String userId;

    @Column(length = 50, nullable = false)
    @NotBlank(message = "Name may not be blank")
    @Size(message="Name cannot be greater than 50", max = 50)
    public String name;

    @Column(length = 1000)
    @Size(message = "Description cannot be greater than 1000", max = 1000)
    public String description;
}
