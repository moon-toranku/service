INSERT INTO project(userid, name, description) VALUES ('123', 'Project 123', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique elit vitae maximus lobortis');
INSERT INTO project(userid, name, description) VALUES ('456', 'Project 456', 'Cras bibendum fringilla feugiat');
INSERT INTO project(userid, name, description) VALUES ('789', 'Project 789', 'Donec quis mauris imperdiet, placerat erat ut, sollicitudin magna');

INSERT INTO category(id, project_userid, name, description, sort, icon) VALUES (0, '123', 'Home', 'Example for homes', 5, 'iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAQAAABKfvVzAAAApklEQVQ4T9XPvREBARiE4ccMESkFUIFRgyaUoY4rQxFGCUYFFEB8mcCsxO85LhDdJt/s7rvBR0vUUSh0mrC7elYiVnpNKAxsRKkUG4MmfGQnjqamjmJn9AufOIi9MRjbi4PJN3zmJLaGj2RoK05mdfhcKdb6b2nfWpTmVXzhLKJbLXRFnC1ew6WLiHzguDUXy7riCeTN1+qvQd1ty6D67Kv/f9BWXQGWiHzXwNJIRgAAAABJRU5ErkJggg==');
INSERT INTO category_tags(category_id, tags) VALUES (0, 'home');
INSERT INTO category_tags(category_id, tags) VALUES (0, 'house');